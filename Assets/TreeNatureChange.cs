﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeNatureChange : MonoBehaviour
{
    GameObject leaf;
    float alpha = 0.3f;
    float red = 0.23f;
    float green = 0.41f;
    float blue = 0.12f;

    // Start is called before the first frame update
    void Start()
    {
        leaf = transform.Find("tree_1_canopy").gameObject;
        leaf.GetComponent<Renderer>().sharedMaterial.SetFloat("_Cutoff", alpha);
        leaf.GetComponent<Renderer>().sharedMaterial.SetVector("_Color", new Vector4(red,green,blue,1f));
        StartCoroutine(TimeChange());

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    IEnumerator TimeChange()
    {
        while (alpha < 1f)
        {
            yield return new WaitForSeconds(3);
            //alpha += 0.008f/alpha;
            alpha = (alpha + 1) / 2;
            print(alpha);
            green -= 0.03f;
            leaf.GetComponent<Renderer>().sharedMaterial.SetFloat("_Cutoff", alpha);
            leaf.GetComponent<Renderer>().sharedMaterial.SetVector("_Color", new Vector4(red,green,blue,1f));
            print("changed");
        }
    }

}
